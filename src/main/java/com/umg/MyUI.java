package com.umg;

import javax.servlet.annotation.WebServlet;
import javax.xml.ws.Service;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import com.vaadin.ui.Grid;

import java.util.Arrays;
import java.util.List;


/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {

    final MyUI person = new MyUI();
        //Formato de Layout(Vertical)
        final VerticalLayout layout = new VerticalLayout();
        final VerticalLayout layout2 = new VerticalLayout();
        final HorizontalLayout hlayout = new HorizontalLayout();
        //Grids
        Grid<Person> grid = new Grid();
        Grid<Curso> grid2 = new Grid();
        Grid<Calificacion> grid3 = new Grid();

        //textos de estudiantes
        final  Label tituloEstudiantes = new Label();
        tituloEstudiantes.setCaption("Registro de Estudiantes");
        final TextField codigoEstudiante = new TextField();
        codigoEstudiante.setCaption("Favor de Ingresar el codigo del Estudiante:");
        final TextField nombreEstudiante = new TextField();
        nombreEstudiante.setCaption("Favor de Ingresar su nombre:");
        //textos de curso
        final  Label tituloCurso = new Label();
        tituloCurso.setCaption("Registro de Curso");
        final TextField codigoCurso = new TextField();
        codigoCurso.setCaption("Favor de Ingresar el codigo del Curso:");
        final TextField nombreCurso = new TextField();
        nombreCurso.setCaption("Favor de Ingresar el nombre del curso:");
        //textos de calificacion
        final  Label tituloCalificacion = new Label();
        tituloCalificacion.setCaption("Registro de Calificacion");
        final TextField codigoEstudiante2 = new TextField();
        codigoEstudiante2.setCaption("Favor de Ingresar el codigo del Estudiante");
        final TextField codigoCurso2 = new TextField();
        codigoCurso2.setCaption("Favor de Ingresar el codigo del curso:");
        final TextField calificacion = new TextField();
        calificacion.setCaption("Favor de Ingresar la calificacion: ");

        Button button = new Button("Agregar Estudiante");
        button.addClickListener( e -> {
            List<Person> people = Arrays.asList(
                            new Person(codigoEstudiante.getValue(), nombreEstudiante.getValue())
                    );
                grid.setItems(people);
                grid.addColumn(Person::getCodigoEstudiante).setCaption("Codigo_Estudiante");
                grid.addColumn(Person::getNombreEstudiante).setCaption("Nombre_Estudiante");
                layout.addComponents(grid);
                setContent(layout);
                layout.setComponentAlignment(grid, Alignment.MIDDLE_CENTER);
        });

        Button button2 = new Button("Agregar Curso");
        button2.addClickListener( e -> {
            List<Curso> curso = Arrays.asList(
                    new Curso(codigoCurso.getValue(), nombreCurso.getValue())
            );
            grid2.setItems(curso);
            grid2.addColumn(Curso::getCodigoCurso).setCaption("Codigo_Curso");
            grid2.addColumn(Curso::getNombreCurso).setCaption("Nombre_Curso");
            layout.addComponents(grid2);
            setContent(layout);
            layout.setComponentAlignment(grid2, Alignment.MIDDLE_CENTER);
        });

        Button button3 = new Button("Agregar Calificacion");
        button3.addClickListener( e -> {
            List<Calificacion> calificacions = Arrays.asList(
                    new Calificacion(codigoEstudiante2.getValue(), codigoCurso2.getValue(), calificacion.getValue())
            );
            grid3.setItems(calificacions);
            grid3.addColumn(Calificacion::getCodigoEstudiante).setCaption("Codigo_Estudiante");
            grid3.addColumn(Calificacion::getCodigoCurso).setCaption("Codigo_Curso");
            grid3.addColumn(Calificacion::getCalificacion).setCaption("Calificacion");
            layout.addComponents(grid3);
            setContent(layout);
            layout.setComponentAlignment(grid3, Alignment.MIDDLE_CENTER);
        });


        //Botones de Menu
        Button buttonA = new Button("Gestion de Estudiantes");
        buttonA.addClickListener( e -> {
            layout.addComponents(tituloEstudiantes, codigoEstudiante, nombreEstudiante, button, grid);
            setContent(layout);
            //Alignment layout
            layout.setComponentAlignment(tituloEstudiantes, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(codigoEstudiante, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(nombreEstudiante, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(button, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(grid, Alignment.MIDDLE_CENTER);
        });
        Button buttonC = new Button("Gestion de Curso");
        buttonC.addClickListener( e -> {
            layout.addComponents(tituloCurso, codigoCurso, nombreCurso, button2, grid2);
            setContent(layout);
            layout.setComponentAlignment(tituloCurso, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(codigoCurso, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(nombreCurso, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(button2, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(grid2, Alignment.MIDDLE_CENTER);
        });
        Button buttonD = new Button("Gestion de Calificacion");
        buttonD.addClickListener( e -> {
            layout.addComponents(tituloCalificacion, codigoEstudiante2, codigoCurso2, calificacion, button3, grid3);
            setContent(layout);
            layout.setComponentAlignment(tituloCalificacion, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(codigoEstudiante2, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(codigoCurso2, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(calificacion, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(button3, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(grid3, Alignment.MIDDLE_CENTER);

        });

        Label bienvenida = new Label("Bienvenidos al Sistema de Informacion de Escuela Informatica.");
        Label bienvenida2 = new Label("Favor de Seleccion una Opcion:");
        bienvenida.addStyleName("v-fontawesome-label-failure");
        layout2.addComponents(bienvenida, bienvenida2);
        layout2.setComponentAlignment(bienvenida, Alignment.MIDDLE_CENTER);
        layout2.setComponentAlignment(bienvenida2, Alignment.MIDDLE_CENTER);

        layout2.addComponents(buttonA);
        layout2.addComponents(buttonC);
        layout2.addComponents(buttonD);
        layout2.setComponentAlignment(buttonA, Alignment.MIDDLE_CENTER);
        layout2.setComponentAlignment(buttonC, Alignment.MIDDLE_CENTER);
        layout2.setComponentAlignment(buttonD, Alignment.MIDDLE_CENTER);
        setContent(layout2);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
