package com.umg;

/**
 * Created by Edward on 14/07/2017.
 */
public class Calificacion {

    private String codigoEstudiante;
    private String codigoCurso;
    private String calificacion;

    public Calificacion(String codigoEstudiante, String codigoCurso, String calificacion) {
        this.codigoEstudiante = codigoEstudiante;
        this.codigoCurso = codigoCurso;
        this.calificacion = calificacion;
    }

    public String getCodigoEstudiante() {
        return codigoEstudiante;
    }

    public void setCodigoEstudiante(String codigoEstudiante) {
        this.codigoEstudiante = codigoEstudiante;
    }

    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }
}
